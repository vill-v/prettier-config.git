# Villv Prettier Config

[![NPM Version](https://img.shields.io/npm/v/%40vill-v%2Fprettier-config?style=flat-square)](https://www.npmjs.com/package/@vill-v/prettier-config)
[![NPM Downloads](https://img.shields.io/npm/dm/%40vill-v%2Fprettier-config?style=flat-square)](https://www.npmjs.com/package/@vill-v/prettier-config)
[![NPM License](https://img.shields.io/npm/l/%40vill-v%2Fprettier-config?style=flat-square)](https://www.npmjs.com/package/@vill-v/prettier-config)


**vill-v** 自用的 `prettier` 配置

## 安装

```shell
pnpm add prettier @vill-v/prettier-config -D
```

## 使用


### 使用帮助方法

prettier.config.([cm])js ESM


```js
import { villv } from '@vill-v/prettier-config/helper'

export default villv()
```

```js
import { defineConfig } from '@vill-v/prettier-config/helper'

export default defineConfig()
```

prettier.config.([cm])js CJS

```js
const { villv } = require('@vill-v/prettier-config/helper')

module.exports = villv()
```

```js
const { defineConfig } = require('@vill-v/prettier-config/helper')

module.exports = defineConfig()
```

### 使用配置项

prettier.config.([cm])js ESM

```js
import config from '@vill-v/prettier-config'

export default config
```

prettier.config.([cm])js CJS

```js
module.exports = require('@vill-v/prettier-config')
```


## 工作环境

- prettier >=3.0.0

## 覆盖配置项

### 使用帮助方法

使用 配置帮助方法 `defineConfig` or `villv` 可以更灵活的配置 `prettier` 选项

你可以像这样在现代编辑器下，可以享受 ts 类型提示带来的体验提升

prettier.config.js 

```js
import { villv } from '@vill-v/prettier-config/helper'

export default villv({
    singleQuote: false,
})
```

### 使用配置项

prettier.config.js 

```js
import villv from '@vill-v/prettier-config'

/** @type {import("prettier").Config} */
export default {
    ...villv,
     singleQuote: false,
}
```

详细的配置项请查看 [Options](https://prettier.io/docs/en/options)

## 其他代码规范

[eslint config](https://gitee.com/vill-v/eslint-config) - eslint flat 风格的配置集合

[prettier config](https://gitee.com/vill-v/prettier-config) - prettier 的默认配置

[tsconfig](https://gitee.com/vill-v/tsconfig) - typescript tsconfig.json 基础配置

## License

[MIT License](./LICENSE)
