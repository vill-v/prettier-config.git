import type { Config } from 'prettier'
import defaultConfig from './config'

export const defineConfig = (config: Config) => ({
  ...defaultConfig,
  ...config,
})

export { defaultConfig as villv }
