import { defineConfig } from 'tsup'

export default defineConfig({
  entry: ['src/index.ts', 'src/helper.ts'],
  dts: { compilerOptions: { baseUrl: '.' } },
  target: 'node16',
  clean: true,
  format: ['cjs', 'esm'],
  treeshake: true,
  splitting: true,
  platform: 'node',
  shims: true,
})
